#include <bits/stdc++.h>

using namespace std;

/*
	Requirements for this code: 
		- GCC with C++11 support.
*/

const int MAX_CLUSTER = 5;
/*
	- input: a vector of real values
	returns: an integer indicating the number of clusters
*/
int count_clusters(vector<double>& input) {
	int nclusters = input.size() > 0;
	for (int i = 1; i < input.size(); i++) {
		if (input[i] >= input[i-1] + 1) {
			nclusters++;
		}
	}
	return nclusters;
}

/*
	- input: a vector of real values
	- n: index of values to process (processing input[n, n+1, ... end-1])
	- m: number of clusters formed
	- k: number of points to remove
	- u: last unremoved index that has not been formed into a cluster
	returns: a length MAX_CLUSTER+1 vector representing p(m | k, S)

	To use:
		vector<double> S; //this is the input
		ClusterSolver solver(S);
		auto result = solver.solve(k); // this returns a MAX_CLUSTER+1 vector representing p(m | k, S)
*/
struct ClusterSolver {
	vector<double> input;
	int N;

	// Forgive this ugly nesting
	vector<vector<vector<vector<double>>>> cache;
	ClusterSolver(vector<double> input_) : input(input_) {
		N = input.size();
		sort(input.begin(), input.end());

		cache.resize(N+1);
		for (int n = 0; n <= N; n++) {
			cache[n].resize(MAX_CLUSTER+1);
			for (int i = 0; i <= MAX_CLUSTER; i++) {
				cache[n][i].resize(N+1);
				for (int j = 0; j <= N; j++) {
					cache[n][i][j].resize(N+1, -1);
				}
			}
		}
	}

	inline double subsolve(int n, int m, int k, int l) {
		// Base cases
		if (N - n == k) {
			if ((m == 1 && l < n) || (m == 0 && l == n)) return 1;
			else return 0;
		}
		if (m < 0) return 0;
		if (cache[n][m][k][l] >= 0) return cache[n][m][k][l];

		double& res = cache[n][m][k][l] = 0;
		double L = N - n;
		if (k > 0) {
			// Remove this point
			if (l == n) res += subsolve(n+1, m, k-1, n+1) * (k/L);
			else res += subsolve(n+1, m, k-1, l) * (k/L);
		}

		// Don't remove this point
		if (input[l] + 1 <= input[n]) {
			// The start of a new chain
			res += subsolve(n+1, m-1, k, n) * (1 - k/L);
		} else {
			res += subsolve(n+1, m, k, n) * (1 - k/L);
		}
		//cerr << n << " " << m << " " << k << " " << l << " " << cache[n][m][k][l] << endl;
		return res;
	}

	vector<double> solve(int k) {
		vector<double> res(MAX_CLUSTER + 1);
		for (int m = 0; m <= MAX_CLUSTER; m++) {
			res[m] = subsolve(0, m, k, 0);
		}
		return res;
	}
};

/*
	Here is some code to generate random point sets and test removing
	all subsets of points.
*/
vector<double> generate_points(int n, double L) {
	default_random_engine generator;
	uniform_real_distribution<double> distribution(0.0, L);
	vector<double> res;
	for (int i = 0; i < n; i++) {
		res.push_back(distribution(generator));
	}
	sort(res.begin(), res.end());
	return res;
}

bool verify(int n, double L) {
	// Generate a random point set of n points from [0, L].
	// Then try removing every possible subset of points
	// and computing the number of clusters.
	if (n > 25) {
		cerr << "Too many points to brute force!" << endl;
		return false;
	}

	vector<double> S = generate_points(n, L);
	vector<vector<double>> results0(n+1);
	for (int k = 0; k <= n; k++) {
		results0[k].resize(MAX_CLUSTER + 1);
	}

	map<int, map<int, double>> binomial;
	for (int s = 0; s < (1<<n); s++) {
		// Iterate through all 2^n subsets and remove the points
		int k = __builtin_popcount(s); // We remove points with bits set to 1
		binomial[n][k]++;
		vector<double> S_;
		for (int i = 0; i < n; i++) {
			if (!(s & (1<<i))) S_.push_back(S[i]);
		}

		int m = count_clusters(S_);
		if (m <= MAX_CLUSTER) {
			results0[k][m] += 1.0;
		}
	}
	for (int k = 0; k <= n; k++) {
		for (int m = 0; m <= MAX_CLUSTER; m++) {
			results0[k][m] /= binomial[n][k];
		}
	}

	ClusterSolver solver(S);
	vector<vector<double>> results1;
	for (int k = 0; k <= n; k++) {
		results1.push_back(solver.solve(k));
	}

	// for (int k = 0; k <= n; k++) {
	// 	for (int m = 0; m <= MAX_CLUSTER; m++) {
	// 		cerr << k << " " << m << ": " << results0[k][m] << " " << results1[k][m] << endl;
	// 	}
	// }

	// Compare the two results
	const double EPS = 1e-6;
	for (int k = 0; k <= n; k++) {
		for (int m = 0; m <= MAX_CLUSTER; m++) {
			if (abs(results0[k][m] - results1[k][m]) > EPS) {
				return false;
			}
		}
	}
	return true;
}

int main() {
	vector<double> S({0.1, 0.2, 0.3, 1.3, 3.5, 3.6, 3.7, 10.0});
	ClusterSolver solver(S);
	for (int k = 0; k <= S.size(); k++) {
		auto result = solver.solve(k);
		cout << k << ": ";
		for (double v : result) {
			cout << fixed << setprecision(4) << v << " ";
		}
		cout << endl;
	}
	cout << endl;

	bool found = false;
	for (int i = 10; i <= 15; i++) {
		cout << "Checking point sets of length " << i << endl;
		for (int L = 1; L <= 100; L++) {
			if (!verify(i, L)) {
				cout << "Discreptancy found for " << i << " points!" << endl;
				found = true;
				break;
			}
		}
		if (found) break;
	}
	cout << "Check completed!" << endl;
	if (!found) cout << "No discreptancy found!" << endl;
	else cout << "Discreptancy found!" << endl;
	cout << endl;

	// Stress test for 150 points
	auto start = clock();
	S = generate_points(150, 75);
	ClusterSolver solver2(S);
	for (int k = 0; k <= S.size(); k++) {
		auto result = solver2.solve(k);
	}
	cout << "Computing all p(m | k, S) for 150 points took " 
		 << 1.0 * (clock() - start) / CLOCKS_PER_SEC << " seconds." << endl;

	return 0;
}