"""
The solve function takes in a set S of 1D coordinates and outputs
the probability distribution p(m | k, S) as described in 
https://math.stackexchange.com/questions/3433910/distribution-of-number-of-clusters-after-randomly-choosing-a-subset-of-points
"""
def solve(S):
	cache = dict()
	"""
	The subsolve function takes in 3 parameters n, m, k.
		- n is the index of the current point to consider (i.e. the last |S|-n points of S)
		- m is the number of clusters we need to achieve in the remaining |S|-n points
		- k is the number of points we've yet to remove
		- l is the index of the last unremoved point
		
	We're going to use dynamic programming (that's what the 'cache' variable is for) 
	to speed up the naive binomial(|S|, k) solution.
	"""
	def subsolve(n, m, k, l):
		if (n, m, k, l) in cache:
			return cache[n, m, k, l]
		
		"""
		If there are only k points left, we must remove all of them, leaving no clusters.
		So the probability is 1 if m == 0, otherwise its 0.
		"""
		if len(S) - n == k:
			if (m == 1 and l < n) or (m == 0 and l == n): return 1
			else: return 0
		
		"""
		If we reach a negative number of clusters thats a 0 probability case.
		"""
		if m < 0:
			return 0
			
		
		"""
		Otherwise, we have a couple options:
			1) We can remove the first point and move on. We compare with the last
			   unremoved point to see if this produces a new cluster.
			2) We can skip the first point and move on, setting the last unremoved point
			   to the current point.
		"""
		# Case for removing the point
		cache[n, m, k, l] = 0
		L = 1.0 * (len(S) - n)
		if k > 0:
			if l == n: 
				cache[n, m, k, l] += subsolve(n+1, m, k-1, n+1) * (1.0 * k / L)
			else:
				cache[n, m, k, l] += subsolve(n+1, m, k-1, l) * (1.0 * k / L)
		
		# Case for not removing the point
		if S[l] + 1 <= S[n]:
			cache[n, m, k, l] += subsolve(n+1, m-1, k, n) * (1 - 1.0 * k / L)
		else:
			cache[n, m, k, l] += subsolve(n+1, m, k, n) * (1 - 1.0 * k / L)

		return cache[n, m, k, l]
	
	# Here we put the entire distribution into a list and return it
	distribution = dict()
	for k in range(len(S)+1):
		distribution[k] = []
		for m in range(len(S)+1):
			distribution[k].append(subsolve(0, m, k, 0))
	
	return distribution
	
S = [0.1, 0.2, 0.3, 1.3, 3.5, 3.6, 3.7, 10.0]
result = solve(S)

for i in range(len(S)+1):
	print i, ", ".join("{0:0.4f}".format(x) for x in result[i][:6])
		
		